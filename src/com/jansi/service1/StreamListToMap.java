package com.jansi.service1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Prod{  
    int id;  
    String name;  
    float price;  
    public Prod(int id, String name, float price) {  
        this.id = id;  
        this.name = name;  
        this.price = price;  
    }  
}  

public class StreamListToMap {
	
    public static void main(String[] args) {  
        List<Prod> productsList = new ArrayList<Prod>();  
  
        
        productsList.add(new Prod(1,"HP Laptop",25000f));  
        productsList.add(new Prod(2,"Dell Laptop",30000f));  
        productsList.add(new Prod(3,"Lenevo Laptop",28000f));  
        productsList.add(new Prod(4,"Sony Laptop",28000f));  
        productsList.add(new Prod(5,"Apple Laptop",90000f));  
          
        // Converting Product List into a Map  
        Map<Integer,String> productPriceMap =   
            productsList.stream()  
                        .collect(Collectors.toMap(p->p.id, p->p.name));  
              
        System.out.println(productPriceMap);  
    }  

}
