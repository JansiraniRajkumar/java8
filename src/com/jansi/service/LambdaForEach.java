package com.jansi.service;

import java.util.ArrayList;
import java.util.List;

public class LambdaForEach {
	
	   public static void main(String[] args) {  
	          
	        List<String> list=new ArrayList<String>();  
	        list.add("Sri");  
	        list.add("Jansi");  
	        list.add("Arun");  
	        list.add("jai");  
	          
	        list.forEach(  
	            (n)->System.out.println(n)  
	        );  
	    }  

}
